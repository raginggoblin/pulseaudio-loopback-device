import Gio from 'gi://Gio';
import Adw from 'gi://Adw';
import Gtk from 'gi://Gtk';

import {ExtensionPreferences, gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';


export default class SoundloopbackPreferences extends ExtensionPreferences {
    fillPreferencesWindow(window) {

        const page = new Adw.PreferencesPage({
            title: _('General'),
            icon_name: 'dialog-information-symbolic',
        });
        window.add(page);

        const group = new Adw.PreferencesGroup({
            title: _('Latency'),
            description: _('Loopback latency in ms.'),
        });
        page.add(group);

        const row = new Gtk.SpinButton();
        row.set_range(0, 100000);
        row.set_increments(1, 1);
        group.add(row);

        window._settings = this.getSettings();
        window._settings.bind('latency-ms', row, 'value', Gio.SettingsBindFlags.DEFAULT);
    }
}
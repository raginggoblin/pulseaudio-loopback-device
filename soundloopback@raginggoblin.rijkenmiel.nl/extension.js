/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import GObject from 'gi://GObject';
import St from 'gi://St';
import GLib from 'gi://GLib';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';
import * as PanelMenu from 'resource:///org/gnome/shell/ui/panelMenu.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';

const Indicator = GObject.registerClass(
    class Indicator extends PanelMenu.Button {

        constructor(settings) {
            super();
            this.settings = settings;
        }

        _init() {
            super._init(0.0, _('Sound Loopback Indicator'));

            let loopbackIcon = new St.Icon({
                style_class: 'sound-loopback-off-icon',
            })
            this.add_child(loopbackIcon);

            let switchLoopback = new PopupMenu.PopupSwitchMenuItem('Loopback', false, {});
            switchLoopback.connect('toggled', (item, state) => {
                loopbackIcon.style_class = state ? 'sound-loopback-on-icon' : 'sound-loopback-off-icon';
                this.setLoopbackActive(state);
            });
            this.menu.addMenuItem(switchLoopback);
        }

        setLoopbackActive(active) {
            if (active) {
                let latency = this.settings.get_int('latency-ms');
                GLib.spawn_command_line_async(`pactl load-module module-loopback latency_msec=${latency}`);
            } else {
                GLib.spawn_command_line_async(`pactl unload-module module-loopback`);
            }
        }
    });

export default class SoundLoopbackExtension extends Extension {
    enable() {
        this._settings = this.getSettings();

        this._indicator = new Indicator(this._settings);
        this._indicator.menu.addAction(_('Preferences...'), () => this.openPreferences());

        Main.panel.addToStatusArea(this.uuid, this._indicator, 1);
    }

    disable() {
        this._indicator.setLoopbackActive(false);
        this._settings = null;
        this._indicator.destroy();
        this._indicator = null;
    }
}

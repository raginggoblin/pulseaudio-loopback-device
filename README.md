# Sound Loopback Device - Pipewire

This is a rewrite of: https://extensions.gnome.org/extension/954/pulseaudio-loopback-device/

Changes from original:
1. Working for Gnome 45, 46 and 47 with Pipewire
2. Using Indicator instead of Button
3. Loopback time is configurable (default 2s)
4. Icons changed to be in line with Adwaita

<p align="center"><img src="https://gitlab.com/raginggoblin/pulseaudio-loopback-device/raw/master/etc/screenshots/tray-inactive.png" /></p>
<p align="center"><img src="https://gitlab.com/raginggoblin/pulseaudio-loopback-device/raw/master/etc/screenshots/tray-active.png" /></p>

### Howto install
1. Check if you can use pactl: command `pactl info` should show something like `Server Name: PulseAudio (on PipeWire 1.2.7)`

#### Gnome Extensions
2. Browse to https://extensions.gnome.org/extension/7577/sound-loopback/
3. Click 'Install'

#### Manually
2. Clone this repo or download a tar/zip containing this repo
3. Copy folder 'soundloopback@raginggoblin.rijkenmiel.nl' to your local extensions dir (~/.local/share/gnome-shell/extensions/)